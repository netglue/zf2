#Net Glue Library Extensions for Zend Framework 2.0

This is a collection of validators, form elements and other bits and bobs that we
use regularly as functional extensions to the Zend Framework library version 2.x

### SYSTEM REQUIREMENTS

Zend Framework 2 requires PHP 5.3.3 or later, so do these extras!

### INSTALLATION

Please see INSTALL.md.
