# ISO 4217 Currency Codes

The XML files in [current-iso-4217-currency-codes.xml](./current-iso-4217-currency-codes.xml)
and [historic-iso-4217-currency-codes.xml](./historic-iso-4217-currency-codes.xml)
were downloaded directly from: [www.currency-iso.org/iso_index/iso_tables.htm](http://www.currency-iso.org/iso_index/iso_tables.htm)

More info on [ISO 4217](http://www.iso.org/iso/home/standards/currency_codes.htm)
