<?php
/**
 * Form View Helper for laying out form controls in a way that is intended to be specific to Twitter Bootstrap
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Form
 * @subpackage ViewHelper
 */


namespace Netglue\Form\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\View\Helper\FormLabel;
use Zend\Form\View\Helper\FormElement;
use Zend\Form\View\Helper\FormElementErrors;
use Zend\Form\ElementInterface;

/**
 * Form View Helper for laying out form controls in a way that is intended to be specific to Twitter Bootstrap
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Form
 * @subpackage ViewHelper
 */
class TwitterBootstrapFormRow extends AbstractHelper {
	
	/**
	 * Whether to render errors or not
	 * @var bool
	 */
	protected $renderErrors = true;
	
	/**
	 * Form Label View Helper
	 * @var FormLabel
	 */
	protected $labelHelper;
	
	/**
	 * Form Element View Helper
	 * @var FormElement
	 */
	protected $elementHelper;
	
	/**
	 * Form Element Errors Helper
	 * @var FormElementErrors
	 */
	protected $elementErrorsHelper;
	
	/**
	 * Wrapper Div Class
	 * @var string
	 */
	protected $controlGroupClass = 'control-group';
	
	/**
	 * Controls Wrapper Div Class
	 * @var string
	 */
	protected $controlsClass = 'controls';
	
	/**
	 * Add this class to all labels
	 * @var string
	 */
	protected $appendLabelClass = 'control-label';
	
	/**
	 * Add this class to all form elements
	 * @var string
	 */
	protected $appendElementClass;
	
	/**
	 * Set the error list to this class
	 * @var string
	 */
	protected $errorClass = 'text-error unstyled';
	
	/**
	 * Class added to input that have an error state
	 * @var string
	 */
	protected $inputErrorClass = 'error';
	
	/**
	 * Class added to the wrapper div when the elment has an error
	 * @var string
	 */
	protected $controlGroupErrorClass = 'error';

	/**
	 * HTML structure format for sprintf
	 * @var string
	 */
	protected $format = '
	<div class="%1$s">
		%2$s
		<div class="%3$s">
			%4$s
			%5$s
		</div>
	</div>
	';
	
	/**
	 * Utility form helper that renders a label (if it exists), an element and errors
	 *
	 * @param ElementInterface $element
	 * @return string
	 * @throws \Zend\Form\Exception\DomainException
	 */
	public function render(ElementInterface $element) {
		
		$errorHelper = $this->getElementErrorsHelper();
		if(NULL !== $this->errorClass) {
			$errorHelper->setAttributes(array('class' => $this->errorClass));
		}
		$errorString = $errorHelper->render($element);

		$isError = (!empty($errorString));
		
		$errorString = $this->renderErrors ? $errorString : '';
		
		$labelHelper = $this->getLabelHelper();
		
		/**
		 * Give the element an ID if it doesn't have one so that
		 * labels work correctly.
		 * AFAIK, on at least recent Safari, clicking a label for an element without an id
		 * will not cause focus.
		 */
		$elementId = ($element->hasAttribute('id') && (string) $element->getAttribute('id') !== '') ? $element->getAttribute('id') : 'id'.uniqid();
		$element->setAttribute('id', $elementId);
		
		/**
		 * Get label attributes from the element and add the specialised classes
		 */
		$labelAttributes = array();
		if($element instanceof \Zend\Form\Element || in_array('getLabelAttributes', get_class_methods($element))) {
			$labelAttributes = $element->getLabelAttributes();
		}
		$labelClass = array();
		if(isset($labelAttributes['class']) && !empty($labelAttributes['class'])) {
			$labelClass[] = $labelAttributes['class'];
		}
		if(!empty($this->appendLabelClass)) {
			$labelClass[] = $this->appendLabelClass;
		}
		$labelClass = trim(implode(' ', $labelClass));
		
		$labelAttributes['for'] = $elementId;
		$labelAttributes['class'] = $labelClass;
		// We can't just setLabelAttributes() because it is not a method in ElementInterface
		// However, inside FormLabel - you'll see that Zend do it...
		$element->setLabelAttributes($labelAttributes);
		
		$labelString = $labelHelper->__invoke($element);
		
		$elementHelper = $this->getElementHelper();
		
		$elementClass = array();
		if($element->hasAttribute('class') && '' !== (string) $element->getAttribute('class')) {
			$elementClass[] = $element->getAttribute('class');
		}
		if($isError && !empty($this->inputErrorClass)) {
			$elementClass[] = $this->inputErrorClass;
		}
		if(!empty($this->appendElementClass)) {
			$elementClass[] = $this->appendElementClass;
		}
		$elementClass = trim(implode(' ', $elementClass));
		$element->setAttribute('class', $elementClass);
		
		$elementString = $elementHelper->render($element);
		
		$wrapperClass = $this->controlGroupClass;
		$wrapperClass .= (!empty($this->controlGroupErrorClass) && $isError) ? ' '.$this->controlGroupErrorClass : '';
		
		return sprintf($this->format,
			$wrapperClass,
			$labelString,
			$this->controlsClass,
			$elementString,
			$errorString);
		
	}
	
	/**
	 * Invoke helper as functor
	 *
	 * Proxies to {@link render()}.
	 *
	 * @param null|ElementInterface $element
	 * @param bool $renderErrors
	 * @return string|TwitterBootstrapFormRow
	 */
	public function __invoke(ElementInterface $element = NULL, $renderErrors = NULL) {
		if (!$element) {
			return $this;
		}

		if ($renderErrors !== null){
			$this->setRenderErrors($renderErrors);
		}

		return $this->render($element);
	}
	
	/**
	 * Are errors rendered by this helper ?
	 *
	 * @param bool $renderErrors
	 * @return TwitterBootstrapFormRow
	 */
	public function setRenderErrors($renderErrors) {
		$this->renderErrors = (bool) $renderErrors;
		return $this;
	}

	/**
	 * Return renderErrors flag
	 * @return bool
	 */
	public function getRenderErrors() {
		return $this->renderErrors;
	}
	
	/**
	 * Retrieve the FormLabel helper
	 *
	 * @return FormLabel
	 */
	public function getLabelHelper() {
		if ($this->labelHelper) {
			return $this->labelHelper;
		}

		if (method_exists($this->view, 'plugin')) {
			$this->labelHelper = $this->view->plugin('form_label');
		}

		if (!$this->labelHelper instanceof FormLabel) {
			$this->labelHelper = new FormLabel();
		}

		if ($this->hasTranslator()) {
			$this->labelHelper->setTranslator(
				$this->getTranslator(),
				$this->getTranslatorTextDomain()
			);
		}

		return $this->labelHelper;
	}

	/**
	 * Retrieve the FormElement helper
	 *
	 * @return FormElement
	 */
	protected function getElementHelper() {
		if ($this->elementHelper) {
			return $this->elementHelper;
		}

		if (method_exists($this->view, 'plugin')) {
			$this->elementHelper = $this->view->plugin('form_element');
		}

		if (!$this->elementHelper instanceof FormElement) {
			$this->elementHelper = new FormElement();
		}

		return $this->elementHelper;
	}

	/**
	 * Retrieve the FormElementErrors helper
	 *
	 * @return FormElementErrors
	 */
	protected function getElementErrorsHelper() {
		if ($this->elementErrorsHelper) {
			return $this->elementErrorsHelper;
		}

		if (method_exists($this->view, 'plugin')) {
			$this->elementErrorsHelper = $this->view->plugin('form_element_errors');
		}

		if (!$this->elementErrorsHelper instanceof FormElementErrors) {
			$this->elementErrorsHelper = new FormElementErrors();
		}

		return $this->elementErrorsHelper;
	}
	
	/**
	 * Set the css class to append to element classes
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setAppendElementClass($class) {
		$this->appendElementClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class string that will be appended to elements
	 * @return string
	 */
	public function getAppendElementClass() {
		return $this->appendElementClass;
	}
	
	/**
	 * Set the css class to append to label classes
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setAppendLabelClass($class) {
		$this->appendLabelClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class string that will be appended to labels
	 * @return string
	 */
	public function getAppendLabelClass() {
		return $this->appendLabelClass;
	}
	
	/**
	 * Set the css class to apply to lists of errors if any
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setErrorClass($class) {
		$this->errorClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class applied to error lists
	 * @return string
	 */
	public function getErrorClass() {
		return $this->errorClass;
	}
	
	/**
	 * Set the css class to apply to form elements with error messages
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setInputErrorClass($class) {
		$this->inputErrorClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class applied to form elements with error messages
	 * @return string
	 */
	public function getInputErrorClass() {
		return $this->inputErrorClass;
	}
	
	/**
	 * Set the css class to apply to the control group wrapper when the element is in an error state
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setControlGroupErrorClass($class) {
		$this->controlGroupErrorClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class applied to the control group wrapper when the element is in an error state
	 * @return string
	 */
	public function getControlGroupErrorClass() {
		return $this->controlGroupErrorClass;
	}
	
	/**
	 * Set class applied to to the wrapper control group
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setControlGroupClass($class) {
		$this->controlGroupClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class applied to to the wrapper control group
	 * @return string
	 */
	public function getControlGroupClass() {
		return $this->controlGroupClass;
	}
	
	/**
	 * Set class applied to to the wrapper around from elements and errors
	 * @param string $class
	 * @return TwitterBootstrapFormRow
	 */
	public function setControlsClass($class) {
		$this->controlsClass = (string) $class;
		return $this;
	}
	
	/**
	 * Return class applied to to the wrapper around from elements and errors
	 * @return string
	 */
	public function getControlsClass() {
		return $this->controlsClass;
	}
	
	/**
	 * Set the format of the HTML string that will be returned
	 *
	 * Your format should use numbered paramters such as %1$s where the indexes correspond to the following
	 * 1: Control Group Wrapper Class
	 * 2: Label
	 * 3: Form Element & Errors Wrapper Div Class
	 * 4: Form Element
	 * 5: Error List
	 *
	 * @param string $format
	 * @return TwitterBootstrapFormRow
	 */
	public function setFormat($format) {
		$this->format = (string) $format;
		return $this;
	}
	
	/**
	 * Return HTML format string
	 * @return string
	 */
	public function getFormat() {
		return $this->format;
	}
	
}
