<?php
/**
 * A Form Element for input of ISO 3166-1 UN Country Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Form
 */


namespace Netglue\Form\Element;

use Zend\Form\Element;
use Zend\InputFilter\InputProviderInterface;
use Netglue\Validator\UnCountryCode as CCValidator;


/**
 * A Form Element for input of ISO 3166-1 UN Country Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Form
 */
class UnCountryCode extends Element implements InputProviderInterface {
	
	/**
	 * Element Attributes
	 * @var array
	 */
	protected $attributes = array(
		'type' => 'text',
		'maxlength' => 3,
		'size' => 5,
		'placeholder' => 'XXX',
		'class' => 'countryCode',
		'title' => '3 Digit (Zero Padded) ISO 3166-1 UN Country Code',
	);
	
	/**
	 * Country Code Validator
	 * @var ValidatorInterface
	 */
	protected $validator;
	
	/**
	 * Get validator
	 * @return ValidatorInterface
	 */
	protected function getValidator() {
		if(null === $this->validator) {
			$this->validator = new CCValidator;
		}
		return $this->validator;
	}
	
	/**
	 * Provide default input rules for this element
	 * Attaches a country code validator
	 *
	 * @return array
	 */
	public function getInputSpecification() {
		return array(
			'name' => $this->getName(),
			'required' => true,
			'filters' => array(
				array('name' => 'Zend\Filter\StringTrim'),
				array(
					'name' => 'Netglue\Filter\StringPad',
					'options' => array(
						'pad_string' => '0',
						'pad_length' => 3,
						'pad_type' => STR_PAD_LEFT,
					),
				),
			),
			'validators' => array(
				$this->getValidator(),
			),
		);
	}
	
}