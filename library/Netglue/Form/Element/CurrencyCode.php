<?php
/**
 * A Form Element for input of ISO 4217 Currency Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Form
 */


namespace Netglue\Form\Element;

use Zend\Form\Element;
use Zend\InputFilter\InputProviderInterface;
use Netglue\Validator\CurrencyCode as CCValidator;


/**
 * A Form Element for input of ISO 4217 Currency Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Form
 */
class CurrencyCode extends Element implements InputProviderInterface {
	
	/**
	 * Element Attributes
	 * @var array
	 */
	protected $attributes = array(
		'type' => 'text',
		'maxlength' => 3,
		'size' => 5,
		'placeholder' => 'XXX',
		'class' => 'currencyCode',
		'title' => '3 letter (Uppercase) ISO 4217 Currency Code',
	);
	
	/**
	 * Currency Code Validator
	 * @var ValidatorInterface
	 */
	protected $validator;
	
	/**
	 * Get validator
	 * @return ValidatorInterface
	 */
	protected function getValidator() {
		if(null === $this->validator) {
			$this->validator = new CCValidator;
		}
		return $this->validator;
	}
	
	/**
	 * Provide default input rules for this element
	 * Attaches a currency code validator
	 *
	 * @return array
	 */
	public function getInputSpecification() {
		return array(
			'name' => $this->getName(),
			'required' => true,
			'filters' => array(
				array('name' => 'Zend\Filter\StringTrim'),
				array('name' => 'Zend\Filter\StringToUpper'),
			),
			'validators' => array(
				$this->getValidator(),
			),
		);
	}
	
}