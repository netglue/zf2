<?php
/**
 * Factory for creating XML Renderer instances
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */


namespace Netglue\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Netglue\View\Renderer\XmlRenderer;

/**
 * Factory for creating XML Renderer instances
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewXmlRendererFactory implements FactoryInterface {
	
	/**
	 * Create and return the XML view renderer
	 *
	 * This method also regeisters all of our required services
	 *
	 * @param  ServiceLocatorInterface $serviceLocator
	 * @return XmlRenderer
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		
		$xmlRenderer = new XmlRenderer();
		
		/**
		 * Add the Xml Template Resolvers to the service locator
		 */
		if(!$serviceLocator->has('NetglueViewXmlResolver')) {
			$serviceLocator->setFactory('NetglueViewXmlResolver', 'Netglue\Mvc\Service\ViewXmlResolverFactory');
			$serviceLocator->setFactory('NetglueViewXmlTemplateMapResolver', 'Netglue\Mvc\Service\ViewXmlTemplateMapResolverFactory');
			$serviceLocator->setFactory('NetglueViewXmlTemplatePathStack', 'Netglue\Mvc\Service\ViewXmlTemplatePathStackFactory');
		}
		
		/**
		 * Make sure the renderer has an instance of the appropriate template resolver
		 */
		$xmlRenderer->setResolver($serviceLocator->get('NetglueViewXmlResolver'));
		
		/**
		 * It also requires access to view helpers
		 */
		$xmlRenderer->setHelperPluginManager(clone $serviceLocator->get('ViewHelperManager'));
		
		return $xmlRenderer;
	}
	
}
