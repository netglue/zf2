<?php
/**
 * Factory for creating Text Rendering Strategy
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */

namespace Netglue\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Netglue\View\Strategy\TextStrategy;

/**
 * Factory for creating Text Rendering Strategy
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewTextStrategyFactory implements FactoryInterface {
	
	/**
	 * Create and return the Text view strategy
	 *
	 * Retrieves the ViewTextRenderer service from the service locator, and
	 * injects it into the constructor for the Text strategy.
	 *
	 * It then attaches the strategy to the View service, at a priority of 100.
	 *
	 * @param  ServiceLocatorInterface $serviceLocator
	 * @return JsonStrategy
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		if(!$serviceLocator->has('Netglue\View\TextRenderer')) {
			$serviceLocator->setFactory('Netglue\View\TextRenderer', 'Netglue\Mvc\Service\ViewTextRendererFactory');
		}
		$txtRenderer = $serviceLocator->get('Netglue\View\TextRenderer');
		$txtStrategy = new TextStrategy($txtRenderer);
		return $txtStrategy;
	}
	
}
