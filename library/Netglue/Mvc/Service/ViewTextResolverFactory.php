<?php
/**
 * Factory for creating Aggregate template resolvers
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */

namespace Netglue\Mvc\Service;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Resolver\AggregateResolver;

/**
 * Factory for creating Aggregate template resolvers
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewTextResolverFactory implements FactoryInterface {
	
	/**
	 * Return Aggregate Template Resolver instance
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return AggregateResolver
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$resolver = new AggregateResolver();
		$resolver->attach($serviceLocator->get('NetglueViewTextTemplateMapResolver'));
		$resolver->attach($serviceLocator->get('NetglueViewTextTemplatePathStack'));
		return $resolver;
	}
	
}
