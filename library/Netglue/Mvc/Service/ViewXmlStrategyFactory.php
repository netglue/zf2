<?php
/**
 * Factory for creating XML Rendering Strategy
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */

namespace Netglue\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Netglue\View\Strategy\XmlStrategy;

/**
 * Factory for creating XML Rendering Strategy
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewXmlStrategyFactory implements FactoryInterface {
	
	/**
	 * Create and return the XML view strategy
	 *
	 * Retrieves the ViewXmlRenderer service from the service locator, and
	 * injects it into the constructor for the XML strategy.
	 *
	 * It then attaches the strategy to the View service, at a priority of 100.
	 *
	 * @param  ServiceLocatorInterface $serviceLocator
	 * @return JsonStrategy
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		if(!$serviceLocator->has('Netglue\View\XmlRenderer')) {
			$serviceLocator->setFactory('Netglue\View\XmlRenderer', 'Netglue\Mvc\Service\ViewXmlRendererFactory');
		}
		$xmlRenderer = $serviceLocator->get('Netglue\View\XmlRenderer');
		$xmlStrategy = new XmlStrategy($xmlRenderer);
		return $xmlStrategy;
	}
	
}
