<?php
/**
 * Factory for creating Text Renderer instances
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */


namespace Netglue\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Netglue\View\Renderer\TextRenderer;

/**
 * Factory for creating Text Renderer instances
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewTextRendererFactory implements FactoryInterface {
	
	/**
	 * Create and return the Text view renderer
	 *
	 * This method also regeisters all of our required services
	 *
	 * @param  ServiceLocatorInterface $serviceLocator
	 * @return XmlRenderer
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		
		$txtRenderer = new TextRenderer();
		
		/**
		 * Add the Text Template Resolvers to the service locator
		 */
		if(!$serviceLocator->has('NetglueViewTextResolver')) {
			$serviceLocator->setFactory('NetglueViewTextResolver', 'Netglue\Mvc\Service\ViewTextResolverFactory');
			$serviceLocator->setFactory('NetglueViewTextTemplateMapResolver', 'Netglue\Mvc\Service\ViewTextTemplateMapResolverFactory');
			$serviceLocator->setFactory('NetglueViewTextTemplatePathStack', 'Netglue\Mvc\Service\ViewTextTemplatePathStackFactory');
		}
		
		/**
		 * Make sure the renderer has an instance of the appropriate template resolver
		 */
		$txtRenderer->setResolver($serviceLocator->get('NetglueViewTextResolver'));
		
		/**
		 * It also requires access to view helpers
		 */
		$txtRenderer->setHelperPluginManager(clone $serviceLocator->get('ViewHelperManager'));
		
		return $txtRenderer;
	}
	
}
