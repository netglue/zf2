<?php
/**
 * Factory for creating template path stack resolvers from view manager config
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */

namespace Netglue\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\View\Resolver as ViewResolver;

/**
 * Factory for creating template path stack resolvers from view manager config
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewTextTemplatePathStackFactory implements FactoryInterface {
	
	/**
	 * Looks in config for a sub key of 'view_manager' named 'xml_template_path_stack'
	 * ['view_manager']['txt_template_path_stack'] => array('path/to/view-scripts')
	 * Additionally, you can specify the key 'txt_template_suffix' to override the
	 * default of .txt.php
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return ViewResolver\TemplatePathStack
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		$stack = array();
		$suffix = 'txt.php';
		if(is_array($config) && isset($config['view_manager'])) {
			$config = $config['view_manager'];
			if(is_array($config) && isset($config['txt_template_path_stack'])) {
				$stack = $config['txt_template_path_stack'];
			}
			if(isset($config['txt_template_suffix'])) {
				$suffix = $config['txt_template_suffix'];
			}
		}
		$templatePathStack = new ViewResolver\TemplatePathStack();
		$templatePathStack->addPaths($stack);
		$templatePathStack->setDefaultSuffix($suffix);
		return $templatePathStack;
	}
	
}
