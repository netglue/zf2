<?php
/**
 * Factory for creating template map resolvers from view manager config
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */

namespace Netglue\Mvc\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\View\Resolver as ViewResolver;

/**
 * Factory for creating template map resolvers from view manager config
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mvc
 */
class ViewXmlTemplateMapResolverFactory implements FactoryInterface {
	
	/**
	 * Looks in config for a sub key of 'view_manager' named 'xml_template_map'
	 * ['view_manager']['xml_template_map'] => array('name-of-view' => 'path/to/script')
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return ViewResolver\TemplateMapResolver
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $serviceLocator->get('Config');
		$map = array();
		if(is_array($config) && isset($config['view_manager'])) {
			$config = $config['view_manager'];
			if(is_array($config) && isset($config['xml_template_map'])) {
				$map = $config['xml_template_map'];
			}
		}
		return new ViewResolver\TemplateMapResolver($map);
	}
	
}
