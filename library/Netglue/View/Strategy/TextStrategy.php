<?php
/**
 * Plain Text View Strategy
 * 
 * 
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */

namespace Netglue\View\Strategy;

use Netglue\View\Model\TextModel;
use Netglue\View\Renderer\TextRenderer;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Request as HttpRequest;
use Zend\View\ViewEvent;

/**
 * Plain Text View Strategy
 * 
 * 
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */
class TextStrategy implements ListenerAggregateInterface {
	
	/**
	 * @var \Zend\Stdlib\CallbackHandler[]
	 */
	protected $listeners = array();
	
	/**
	 * @var TextRenderer
	 */
	protected $renderer;
	
	/**
	 * Constructor
	 *
	 * @param  TextRenderer $renderer
	 */
	public function __construct(TextRenderer $renderer) {
		$this->renderer = $renderer;
	}
	
	/**
	 * Attach the aggregate to the specified event manager
	 *
	 * @param  EventManagerInterface $events
	 * @param  int $priority
	 * @return void
	 */
	public function attach(EventManagerInterface $events, $priority = 1) {
		$this->listeners[] = $events->attach(ViewEvent::EVENT_RENDERER, array($this, 'selectRenderer'), $priority);
		$this->listeners[] = $events->attach(ViewEvent::EVENT_RESPONSE, array($this, 'injectResponse'), $priority);
	}
	
	/**
	 * Detach aggregate listeners from the specified event manager
	 *
	 * @param  EventManagerInterface $events
	 * @return void
	 */
	public function detach(EventManagerInterface $events) {
		foreach($this->listeners as $index => $listener) {
			if($events->detach($listener)) {
				unset($this->listeners[$index]);
			}
		}
	}
	
	/**
	 * Detect if we should use the TextRenderer based on model type
	 * This method could be extended to search header for appropriate accept values
	 *
	 * @param  ViewEvent $e
	 * @return null|TextRenderer
	 */
	public function selectRenderer(ViewEvent $e) {
		$model = $e->getModel();
		if(!$model instanceof TextModel) {
			// no TextModel; do nothing
			return;
		}
		// TextModel found
		return $this->renderer;
	}
	
	/**
	 * Inject the response with the Plain Text payload and appropriate Content-Type header
	 *
	 * @param  ViewEvent $e
	 * @return void
	 */
	public function injectResponse(ViewEvent $e) {
		$renderer = $e->getRenderer();
		if ($renderer !== $this->renderer) {
			// Discovered renderer is not ours; do nothing
			return;
		}
		$result = $e->getResult();
		if(!is_string($result)) {
			// We don't have a string, and thus, no Text
			return;
		}
		$model = $e->getModel();
		// Populate response
		$response = $e->getResponse();
		$response->setContent($result);
		$headers = $response->getHeaders();
		
		$charset = '';
		if(!is_null($model->getEncoding())) {
			$charset = '; charset=' . $model->getEncoding();
		}
		$contentType = 'text/plain';
		if(!is_null($model->getContentType())) {
			$contentType = $model->getContentType();
		}
		
		$headers->addHeaderLine('content-type', $contentType.$charset);
	}
	
}
