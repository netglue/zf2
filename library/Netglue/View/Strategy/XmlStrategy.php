<?php
/**
 * XML View Strategy
 * 
 * 
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */

namespace Netglue\View\Strategy;

use Netglue\View\Model\XmlModel;
use Netglue\View\Renderer\XmlRenderer;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Request as HttpRequest;
use Zend\View\ViewEvent;

/**
 * XML View Strategy
 * 
 * 
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */
class XmlStrategy implements ListenerAggregateInterface {
	
	/**
	 * @var \Zend\Stdlib\CallbackHandler[]
	 */
	protected $listeners = array();
	
	/**
	 * @var XmlRenderer
	 */
	protected $renderer;
	
	/**
	 * Constructor
	 *
	 * @param  XmlRenderer $renderer
	 */
	public function __construct(XmlRenderer $renderer) {
		$this->renderer = $renderer;
	}
	
	/**
	 * Attach the aggregate to the specified event manager
	 *
	 * @param  EventManagerInterface $events
	 * @param  int $priority
	 * @return void
	 */
	public function attach(EventManagerInterface $events, $priority = 1) {
		$this->listeners[] = $events->attach(ViewEvent::EVENT_RENDERER, array($this, 'selectRenderer'), $priority);
		$this->listeners[] = $events->attach(ViewEvent::EVENT_RESPONSE, array($this, 'injectResponse'), $priority);
	}
	
	/**
	 * Detach aggregate listeners from the specified event manager
	 *
	 * @param  EventManagerInterface $events
	 * @return void
	 */
	public function detach(EventManagerInterface $events) {
		foreach($this->listeners as $index => $listener) {
			if($events->detach($listener)) {
				unset($this->listeners[$index]);
			}
		}
	}
	
	/**
	 * Detect if we should use the XmlRenderer based on model type
	 * This method could be extended to search header for appropriate accept values
	 *
	 * @param  ViewEvent $e
	 * @return null|XmlRenderer
	 */
	public function selectRenderer(ViewEvent $e) {
		$model = $e->getModel();
		if(!$model instanceof XmlModel) {
			// no XmlModel; do nothing
			return;
		}
		// XmlModel found
		return $this->renderer;
	}
	
	/**
	 * Inject the response with the XML payload and appropriate Content-Type header
	 *
	 * @param  ViewEvent $e
	 * @return void
	 */
	public function injectResponse(ViewEvent $e) {
		$renderer = $e->getRenderer();
		if ($renderer !== $this->renderer) {
			// Discovered renderer is not ours; do nothing
			return;
		}
		$result = $e->getResult();
		if(!is_string($result)) {
			// We don't have a string, and thus, no XML
			return;
		}
		$model = $e->getModel();
		// Populate response
		$response = $e->getResponse();
		$response->setContent($result);
		$headers = $response->getHeaders();
		
		$charset = '';
		if(!is_null($model->getEncoding())) {
			$charset = '; charset=' . $model->getEncoding();
		}
		$contentType = 'application/xml';
		if(!is_null($model->getContentType())) {
			$contentType = $model->getContentType();
		}
		
		$headers->addHeaderLine('content-type', $contentType.$charset);
	}
	
}
