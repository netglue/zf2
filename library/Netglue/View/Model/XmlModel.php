<?php
/**
 * XML View Model
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */

namespace Netglue\View\Model;

use Zend\View\Model\ViewModel;

/**
 * XML View Model
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */
class XmlModel extends ViewModel {
	
	/**
	 * XML probably won't need to be captured into a
	 * a parent container by default.
	 *
	 * @var string
	 */
	protected $captureTo = null;
	
	/**
	 * XML is usually terminal
	 *
	 * @var bool
	 */
	protected $terminate = true;
	
	/**
	 * UTF-8 Default Encoding
	 * @var string
	 */
	protected $encoding = 'utf-8';
	
	/**
	 * Content Type Header
	 * @var string
	 */
	protected $contentType = 'application/xml';
	
	/**
	 * Set Encoding
	 * @param string $encoding
	 * @return XmlModel
	 */
	public function setEncoding($encoding) {
		$this->encoding = $encoding;
		return $this;
	}
	
	/**
	 * Get Encoding
	 * @return string
	 */
	public function getEncoding() {
		return $this->encoding;
	}
	
	/**
	 * Set Content Type
	 * @param string $contentType
	 * @return XmlModel
	 */
	public function setContentType($contentType) {
		$this->encoding = $contentType;
		return $this;
	}
	
	/**
	 * Get Content Type
	 * @return string
	 */
	public function getContentType() {
		return $this->contentType;
	}
	
}
