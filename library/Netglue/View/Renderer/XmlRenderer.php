<?php
/**
 * XML Renderer
 * 
 * This is just an empty extension of the default PHP renderer. So XML
 * view scripts will behave just like normal
 * 
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */

namespace Netglue\View\Renderer;
use Zend\View\Renderer\PhpRenderer;

/**
 * XML Renderer
 * 
 * This is just an empty extension of the default PHP renderer. So XML
 * view scripts will behave just like normal
 * 
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_View
 */
class XmlRenderer extends PhpRenderer {
	
	
}
