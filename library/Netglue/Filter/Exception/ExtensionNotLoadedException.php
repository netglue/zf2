<?php
/**
 * Filter Exception
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Filter
 * @subpackage Exception
 */

namespace Netglue\Filter\Exception;

/**
 * Filter Exception
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Filter
 * @subpackage Exception
 */
class ExtensionNotLoadedException extends RuntimeException {

}