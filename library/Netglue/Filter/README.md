#Filters

All filters extend `Zend\Filter\AbstractFilter`

## HtmlToText

_(The only one here so far!)_ It's pretty straight forward to use and there's not
any config options that aren't easily figured out from the code.

* Currently, link conversion is very weak. But, to make it better, I'd need more config I suspect.
* There are a few tests, but they're a bit lacking
* Encoding is a little bit mystical... More work needs to be done to make sure intl. characters are preserved
* Not yet tested on a wide variety of HTML sources, which would no doubt reveal potential shortcomings...

On the plus side, for my purposes, it's just fine and dandy and nicely converts 
HTML email messages to a pretty reasonable plain text alternative!
