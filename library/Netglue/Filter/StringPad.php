<?php
/**
 * Implements str_pad as a filter
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Filter
 */

namespace Netglue\Filter;

use Zend\Filter\AbstractFilter;

/**
 * Implements str_pad as a filter
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Filter
 */
class StringPad extends AbstractFilter {
	
	/**
	 * Filter Options
	 *
	 * @var array
	 */
	protected $options = array(
		'pad_length' => NULL,
		'pad_string' => ' ',
		'pad_type' => STR_PAD_RIGHT,
	);
	
	/**
	 * Constructor
	 * Valid Options are:
	 *  'pad_length' => Pad Length
	 *  'pad_string' => String to pad with
	 *  'pad_type' => PHP STR_PAD_* Constant
	 * @link http://php.net/str_pad
	 * @param array|Traversable $options
	 * @return void
	 */
	public function __construct($options = NULL) {
		if(NULL !== $options && static::isOptions($options)) {
			$this->setOptions($options);
		}
	}
	
	/**
	 * Filter
	 * @param string $value
	 * @return string
	 */
	public function filter($value) {
		$value = "$value";
		return str_pad($value, $this->options['pad_length'], $this->options['pad_string'], $this->options['pad_type']);
	}
	
	/**
	 * Set Pad Type
	 * @param int|string $type
	 * @return StringPad (Fluid)
	 */
	public function setPadType($type) {
		if(is_int($type)) {
			$this->options['pad_type'] = $type;
			return $this;
		}
		$value = strtolower($type);
		$option = STR_PAD_RIGHT;
		switch($value) {
			default:
			case 'right':
				break;
			
			case 'left':
				$option = STR_PAD_LEFT;
				break;
			
			case 'both':
				$option = STR_PAD_BOTH;
				break;
		}
		$this->options['pad_type'] = $option;
		return $this;
	}
	
	/**
	 * Return pad type
	 * @return int
	 */
	public function getPadType() {
		return $this->options['pad_type'];
	}
	
	/**
	 * Set Pad String
	 * @param string $str
	 * @return StringPad (Fluid)
	 */
	public function setPadString($str) {
		$this->options['pad_string'] = (string) $str;
		return $this;
	}
	
	/**
	 * Return pad string
	 * @return string
	 */
	public function getPadString() {
		return $this->options['pad_string'];
	}
	
	/**
	 * Set Pad Length
	 * @param int $length
	 * @return StringPad (Fluid)
	 */
	public function setPadLength($length) {
		$this->options['pad_length'] = (int) $length;
		return $this;
	}
	
	/**
	 * Return pad length
	 * @return int
	 */
	public function getPadLength() {
		return $this->options['pad_length'];
	}
	
}
