<?php
/**
 * Rudimentary filter for converting HTML to plain text
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Filter
 */

namespace Netglue\Filter;

use Traversable;
use Zend\Filter\AbstractFilter;

/**
 * Rudimentary filter for converting HTML to plain text
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Filter
 */
class HtmlToText extends AbstractFilter {
	
	/**
	 * Filter Options
	 * @var array
	 */
	protected $options = array(
		'width' => 70,
		'linkListTitle' => 'Links Index',
		'convertLinks' => false,
		'hostPrepend' => NULL,
		'allowLocalLinks' => false,
	);
	
	protected static $_ignore = array(
		'html', 'head', 'body', 'style', 'title', 'meta', 'script',
	);
	
	protected $anchors = array();
	
	/**
	 * Constructor
	 * @param array|Traversable $options
	 * @return void
	 */
	public function __construct($options = NULL) {
		if(NULL !== $options && static::isOptions($options)) {
			$this->setOptions($options);
		}
	}
	
	/**
	 * Filter/Convert Html
	 * @param string $html
	 * @return string
	 */
	public function filter($html) {
		$this->anchors = array();
		if(!is_string($html)) {
			throw new Exception\InvalidArgumentException('Expected a string to filter');
		}
		if(!class_exists('\DomDocument')) {
			throw new Exception\ExtensionNotLoadedException('The DOM extension is not loaded');
		}
		if(!function_exists('mb_detect_encoding')) {
			throw new Exception\ExtensionNotLoadedException('Multibyte extension is required to function');
		}
		$encoding = mb_detect_encoding($html);
		$prepend = '<?xml encoding="'.$encoding.'">';
		$html = str_replace(array("\r\n", "\r"), "\n", $html);
		$dom = new \DomDocument;
		// Suppress probably myriad errors
		if(false === @$dom->loadHTML($prepend.$html)) {
			throw new Exception\InvalidArgumentException('Failed to load HTML');
		}
		
		if($this->options['convertLinks'] === true && !empty($this->options['hostPrepend'])) {
			$this->convertLinks($dom);
		}
		
		$page = $this->node($dom);
		$page .= $this->buildLinks();
		return mb_convert_encoding($page, $encoding, 'UTF-8');
	}
	
	/**
	 * Prepend a host and path to links presumed to be relative
	 * @param \DomDocument $dom
	 * @return void
	 */
	protected function convertLinks(\DomDocument $dom) {
		foreach($dom->getElementsByTagName('a') as $link) {
			$href = $link->getAttribute('href');
			if(preg_match("/^(\.|\/)/", $href)) {
				$href = $this->options['hostPrepend'].$href;
				$link->setAttribute('href', $href);
			}
		}
	}
	
	/**
	 * Return a list of numbered links to append to the text document
	 * @return string
	 */
	protected function buildLinks() {
		if(!count($this->anchors)) {
			return '';
		}
		$out = array();
		$out[] = wordwrap($this->options['linkListTitle'], $this->options['width']);
		$out[] = str_repeat('-', $this->options['width']);
		$out[] = '';
		foreach($this->anchors as $key => $href) {
			$out[] = sprintf('[%d] %s', $key, $href);
		}
		return "\n".implode("\n", $out)."\n";
	}
	
	/**
	 * Iteration utility that loops over dom nodes and returns a text representation
	 * @param mixed $node
	 * @param int $width
	 * @return string
	 */
	protected function node($node, $width = NULL) {
		
		if(NULL === $width) {
			$width = $this->options['width'];
		}
		
		/**
		 * This is a text node
		 * strip runs of whitespace and return the text
		 */
		if($node instanceof \DomText) {
			$text = preg_replace("/\s+/", " ", $node->wholeText);
			return wordwrap($text, $width);
		}
		
		/**
		 * Skip Comments
		 */
		if($node instanceof \DomComment) {
			return '';
		}
		if($node instanceof \DomDocumentType) {
			return '';
		}
		if($node instanceof \DOMProcessingInstruction) {
			return '';
		}
		
		$name = strtolower($node->nodeName);
		
		// Skip these completely
		if(in_array($name, array("style", "head", "title", "meta", "script", "form", "img"))) {
			return '';
		}
		
		/**
		 * Add Leading Whitespace to Block Level Elements
		 */
		$output = $this->leadingWhitespace($node);
		
		if($node instanceof \DomElement) {
			if(preg_match('/^h[0-9]{1}$/', $name)) {
				return $output.$this->heading($node, $width).$this->trailingWhitespace($node);
			}
		}
		
		
		switch($name) {
			case 'address':
				return $output.$this->address($node, $width).$this->trailingWhitespace($node);
				break;
			case 'br':
				return "\n";
				break;
			case 'ol':
			case 'ul':
				return $output.$this->htmlList($node, $width).$this->trailingWhitespace($node);
				break;
			case 'li':
				return $output.$this->listItem($node, $width).$this->trailingWhitespace($node);
				break;
			case 'hr':
				return $output.str_repeat('-', $width).$this->trailingWhitespace($node);
				break;
			case "p":
				return $output.$this->paragraph($node, $width);
				break;
			case "a":
				return $this->anchor($node, $width);
				break;
			case "em":
			case "i":
				break;
		}
		
		
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $width);
		}
		$text = preg_replace("/^\n[\s]+/", "\n", $text);
		$trim = trim($text);
		if(empty($trim)) {
			return '';
		}
		$output .= $text.$this->trailingWhitespace($node);
		
		return $output;
	}
	
	/**
	 * Selectively return whitespace depending on the type of node provided
	 * @param mixed $node
	 * @return string
	 */
	protected function leadingWhitespace($node) {
		$name = strtolower($node->nodeName);
		if($node instanceof \DomElement) {
			if(preg_match('/^h[0-9]{1}$/', $name)) {
				return "\n";
			}
		}
		$blockStart = array(
			'p','ol','ul','dl','dt','dd','li','br','hr',
		);
		if(in_array($name, $blockStart) && $node instanceof \DomElement) {
			return "\n";
		}
		return '';
	}
	
	/**
	 * Selectively return whitespace depending on the type of node provided
	 * @param mixed $node
	 * @return string
	 */
	protected function trailingWhitespace($node) {
		$name = strtolower($node->nodeName);
		$blockStart = array(
			'p','dl',
		);
		if(in_array($name, $blockStart) && $node instanceof \DomElement) {
			return "\n";
		}
		return '';
	}
	
	/**
	 * Specialized method for converting a p element to plain text
	 * @param \DomElement $node
	 * @return string
	 */
	protected function paragraph(\DomElement $node, $width) {
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $width);
		}
		$text = str_replace("\n", ' ', $text);
		$text = preg_replace("/[\s]+/", ' ', $text);
		$text = trim(wordwrap($text, $width));
		if(empty($text)) {
			return '';
		}
		$text = preg_replace("/\n[\s]+/", "\n", $text).$this->trailingWhitespace($node);
		return $text;
	}
	
	/**
	 * Specialized method for converting an address element to plain text
	 * @param \DomElement $node
	 * @return string
	 */
	protected function address(\DomElement $node, $width) {
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $width);
		}
		return $text;
	}
	
	/**
	 * Specialized method for converting an anchor element to plain text
	 * @param \DomElement $node
	 * @return string
	 */
	protected function anchor(\DomElement $node, $width) {
		$href = $node->getAttribute('href');
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $width);
		}
		$trim = trim($text);
		if(empty($text)) {
			return '';
		}
		if(empty($href) || $href == '#' || preg_match('/^java/', $href)) {
			return $text;
		}
		if(false === $this->options['allowLocalLinks']) {
			if(!preg_match('/^([a-z]+:\/\/|(mail|call)to:)/', $href)) {
				return $text;
			}
		}
		
		if(!count($this->anchors)) {
			$key = 1;
		} else {
			$key = array_search($href, $this->anchors);
			if(false === $key) {
				$key = count($this->anchors) + 1;
			}
		}
		$this->anchors[$key] = $href;
		$text .= " [{$key}]";
		return $text;
	}
	
	/**
	 * Format Heading Elements
	 * @param \DomElement $node
	 * @param int $width
	 * @return string
	 */
	protected function heading(\DomElement $node, $width) {
		$pad = '#';
		$padLen = 4;
		//$width = $this->options['width'];
		$maxWidth = $width - $padLen;
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $maxWidth);
		}
		$text = trim(str_replace("\n", " ", $text));
		$lines = explode("\n", wordwrap($text, $maxWidth));
		foreach($lines as &$line) {
			if(count($lines) > 1) {
				$line = str_pad($line, $maxWidth, ' ');
			}
			$line = sprintf("%s%s%s%s%s", $pad, ' ', $line, ' ', $pad);
		}
		return implode("\n", $lines);
	}
	
	/**
	 * Format List Elements
	 * @param \DomElement $node
	 * @param int $width
	 * @return string
	 */
	protected function htmlList(\DomElement $node, $width) {
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $width - 1);
		}
		return str_replace("\n\n", "\n", preg_replace("/^[\s]*$/m", "", $text));
		return $text;
	}
	
	/**
	 * Format List Item Elements
	 * @param \DomElement $node
	 * @param int $width
	 * @return string
	 */
	protected function listItem(\DomElement $node, $width) {
		$indent = $this->options['width'] - $width;
		$lpad = str_repeat(' ', $indent);
		$maxWidth = $width - ($indent + 2);
		$text = '';
		for($i = 0; $i < $node->childNodes->length; $i++) {
			$child = $node->childNodes->item($i);
			$text .= $this->node($child, $width);
		}
		$trim = trim($text);
		if(empty($trim)) {
			return '';
		}
		$lines = explode("\n", wordwrap(trim($text), $width));
		$out = array($lpad.'* '.current($lines));
		if(count($lines) > 1) {
			unset($lines[key($lines)]);
			foreach($lines as $line) {
				$out[] = $lpad.$line;
			}
		}
		return implode("\n", $out);
	}
	
}