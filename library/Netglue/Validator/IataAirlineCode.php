<?php
/**
 * Validation for IATA Airline Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Validator
 */

namespace Netglue\Validator;
use Zend\Validator\AbstractValidator AS AbstractValidator;

/**
 * Validation for IATA Airline Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Validator
 * @link http://en.wikipedia.org/wiki/International_Air_Transport_Association_airport_code
 */
class IataAirlineCode extends AbstractValidator {
	
	/**
	 * Error: Not String
	 */
	const INVALID    = 'notString';
	
	/**
	 * Error: Regex doesn't match
	 */
	const BAD_SYNTAX = 'badSyntax';
	
	/**
	 * Error: Empty String
	 */
	const STR_EMPTY  = 'emptyString';
	
	/**
	 * Error: String length too long
	 */
	const TOO_LONG   = 'tooLong';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => 'IATA Airline codes should be an alphanumeric string',
		self::STR_EMPTY => 'I received an empty string. Please input the IATA Code',
		self::BAD_SYNTAX => 'IATA airline codes should be two or three letters or digits. Letters should be uppercase',
		self::TOO_LONG => 'IATA airline codes are a maximum of 3 characters long',
	);
	
	/**
	 * Returns true if $value matches a regex for an IATA Airline Code
	 *
	 * @param string $value
	 * @return boolean
	 */
	public function isValid($value) {
		if(!is_string($value)) {
			$this->error(self::INVALID);
			return false;
		}
		$this->setValue($value);
		if(empty($value)) {
			$this->error(self::STR_EMPTY);
		}
		if(strlen($value) > 3) {
			$this->error(self::TOO_LONG);
		}
		if(!preg_match('/^[A-Z0-9]{2}[A-Z]{0,1}$/', $value)) {
			$this->error(self::BAD_SYNTAX);
		}
		if(count($this->getMessages())) {
			return false;
		} else {
			return true;
		}
	}
	
}