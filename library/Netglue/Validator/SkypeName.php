<?php
/**
 * Validation for Skype Usernames
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Validator
 */

namespace Netglue\Validator;
use Zend\Validator\AbstractValidator AS AbstractValidator;

/**
 * Validation for Skype Usernames
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Validator
 */
class SkypeName extends AbstractValidator {
	
	/**
	 * Regex for skype name validation
	 */
	const PATTERN = "/^[a-z][a-z0-9_\-\,\.]{5,31}$/i";
	
	/**
	 * Error: Not a string
	 */
	const INVALID    = 'notString';
	
	/**
	 * Error: Empty string
	 */
	const STR_EMPTY  = 'emptyString';
	
	/**
	 * Error: Invalid
	 */
	const PATTERN_FAIL = 'regexNotMatch';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => "Skype names should be alpha numeric strings",
		self::STR_EMPTY => "I received an empty string. Please provide a skype username",
		self::PATTERN_FAIL => "Skype names must start with a letter and be between 6 and 32 characters long. Only letters, numbers, underscrore, dot, hyphen and comma are allowed",
	);
	
	/**
	 * Validate
	 * @return bool
	 * @param string $str
	 */
	public function isValid($str) {
		if(!is_string($str)) {
			$this->error(self::INVALID);
			return false;
		}
		if(empty($str)) {
			$this->error(self::STR_EMPTY);
			return false;
		}
		
		$this->setValue($value);
		
		if(!preg_match(self::PATTERN, $str)) {
			$this->error(self::PATTERN_FAIL);
			return false;
		}
		return true;
	}
	
}