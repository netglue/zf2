<?php
/**
 * Validation for ISO 3166-1 UN Country Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Validator
 */

namespace Netglue\Validator;
use Zend\Validator\AbstractValidator AS AbstractValidator;

/**
 * Validation for ISO 3166-1 UN Country Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Validator
 */
class UnCountryCode extends AbstractValidator {
	
	/**
	 * Regex to test currency code
	 */
	const PATTERN = '/^[0-9]{3}$/';
	
	/**
	 * Error: Not String
	 */
	const INVALID = 'notString';
	
	/**
	 * Error: Fails Regex
	 */
	const NOT_MATCH = 'noMatch';
	
	/**
	 * Error: Code does not exist
	 */
	const NOT_FOUND = 'notFound';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => "Invalid type given. String expected",
		self::NOT_MATCH => "Invalid country code value. 3 digits expected",
	);
	
	/**
	 * Returns true if $value matches a regex for an ISO 3166-1 UN Country Codes
	 *
	 * @param string $value
	 * @return boolean
	 */
	public function isValid($value) {
		if(!is_string($value)) {
			$this->error(self::INVALID);
			return false;
		}
		
		$this->setValue($value);
		
		if(!preg_match(self::PATTERN, $value)) {
			$this->error(self::NOT_MATCH);
		}
		
		if(count($this->getMessages())) {
			return false;
		} else {
			return true;
		}
	}
	
}