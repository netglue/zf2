<?php
/**
 * Validation for ISO 4217 Currency Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Validator
 */

namespace Netglue\Validator;
use Zend\Validator\AbstractValidator AS AbstractValidator;

/**
 * Validation for ISO 4217 Currency Codes
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Validator
 */
class CurrencyCode extends AbstractValidator {
	
	/**
	 * Regex to test currency code
	 */
	const PATTERN = '/^[A-Z]{3}$/';
	
	/**
	 * Error: Not String
	 */
	const INVALID = 'notString';
	
	/**
	 * Error: Fails Regex
	 */
	const NOT_MATCH = 'noMatch';
	
	/**
	 * Error: Code does not exist
	 */
	const NOT_FOUND = 'notFound';
	
	/**
	 * Error Message Templates
	 * @var array
	 */
	protected $messageTemplates = array(
		self::INVALID => "Invalid type given. String expected",
		self::NOT_MATCH => "Invalid currency code value. 3 uppercase letters expected",
		self::NOT_FOUND => "The currency code provided does not match any known currency code",
	);
	
	/**
	 * Valid Current Currency Codes
	 * @var array
	 */
	protected $currentCodes = array(
		'AED', 'AFN', 'ALL', 'AMD', 'ANG', 'AOA', 'ARS', 'AUD', 'AWG', 'AZN',
		'BAM', 'BBD', 'BDT', 'BGN', 'BHD', 'BIF', 'BMD', 'BND', 'BOB', 'BOV',
		'BRL', 'BSD', 'BTN', 'BWP', 'BYR', 'BZD', 'CAD', 'CDF', 'CHE', 'CHF',
		'CHW', 'CLF', 'CLP', 'CNY', 'COP', 'COU', 'CRC', 'CUC', 'CUP', 'CVE',
		'CZK', 'DJF', 'DKK', 'DOP', 'DZD', 'EGP', 'ERN', 'ETB', 'EUR', 'FJD',
		'FKP', 'GBP', 'GEL', 'GHS', 'GIP', 'GMD', 'GNF', 'GTQ', 'GYD', 'HKD',
		'HNL', 'HRK', 'HTG', 'HUF', 'IDR', 'ILS', 'INR', 'IQD', 'IRR', 'ISK',
		'JMD', 'JOD', 'JPY', 'KES', 'KGS', 'KHR', 'KMF', 'KPW', 'KRW', 'KWD',
		'KYD', 'KZT', 'LAK', 'LBP', 'LKR', 'LRD', 'LSL', 'LTL', 'LVL', 'LYD',
		'MAD', 'MDL', 'MGA', 'MKD', 'MMK', 'MNT', 'MOP', 'MRO', 'MUR', 'MVR',
		'MWK', 'MXN', 'MXV', 'MYR', 'MZN', 'NAD', 'NGN', 'NIO', 'NOK', 'NPR',
		'NZD', 'OMR', 'PAB', 'PEN', 'PGK', 'PHP', 'PKR', 'PLN', 'PYG', 'QAR',
		'RON', 'RSD', 'RUB', 'RWF', 'SAR', 'SBD', 'SCR', 'SDG', 'SEK', 'SGD',
		'SHP', 'SLL', 'SOS', 'SRD', 'SSP', 'STD', 'SVC', 'SYP', 'SZL', 'THB',
		'TJS', 'TMT', 'TND', 'TOP', 'TRY', 'TTD', 'TWD', 'TZS', 'UAH', 'UGX',
		'USD', 'USN', 'USS', 'UYI', 'UYU', 'UZS', 'VEF', 'VND', 'VUV', 'WST',
		'XAF', 'XAG', 'XAU', 'XBA', 'XBB', 'XBC', 'XBD', 'XCD', 'XDR', 'XFU',
		'XOF', 'XPD', 'XPF', 'XPT', 'XSU', 'XTS', 'XUA', 'XXX', 'YER', 'ZAR',
		'ZMK', 'ZWL'
	);
	
	/**
	 * Historic Currency Codes
	 * @var array
	 */
	protected $historicCodes = array(
		'ADP', 'AFA', 'ALK', 'ANG', 'AOK', 'AON', 'AOR', 'ARA', 'ARP', 'ARY',
		'ATS', 'AYM', 'AZM', 'BAD', 'BEC', 'BEF', 'BEL', 'BGJ', 'BGK', 'BGL',
		'BOP', 'BRB', 'BRC', 'BRE', 'BRN', 'BRR', 'BUK', 'BYB', 'CHC', 'CNX',
		'CSD', 'CSJ', 'CSK', 'CYP', 'DDM', 'DEM', 'ECS', 'ECV', 'EEK', 'EQE',
		'ESA', 'ESB', 'ESP', 'EUR', 'FIM', 'FRF', 'GEK', 'GHC', 'GHP', 'GNE',
		'GNS', 'GQE', 'GRD', 'GWE', 'GWP', 'HRD', 'IDR', 'IEP', 'ILP', 'ILR',
		'ISJ', 'ITL', 'LAJ', 'LSM', 'LTT', 'LUC', 'LUF', 'LUL', 'LVR', 'MAF',
		'MGF', 'MLF', 'MTL', 'MTP', 'MVQ', 'MXP', 'MZE', 'MZM', 'NIC', 'NLG',
		'PEH', 'PEI', 'PES', 'PLZ', 'PTE', 'RHD', 'ROK', 'ROL', 'RUR', 'SDD',
		'SDP', 'SIT', 'SKK', 'SRG', 'SUR', 'TJR', 'TMM', 'TPE', 'TRL', 'TRY',
		'UAK', 'UGS', 'UGW', 'UYN', 'UYP', 'VEB', 'VEF', 'VNC', 'XEU', 'XFO',
		'XRE', 'YDD', 'YUD', 'YUM', 'YUN', 'ZAL', 'ZMK', 'ZRN', 'ZRZ', 'ZWC',
		'ZWD', 'ZWN', 'ZWR'
	);
	
	/**
	 * Returns true if $value matches a regex for an ISO 4217 Currency Code
	 *
	 * @param string $value
	 * @return boolean
	 */
	public function isValid($value) {
		if(!is_string($value)) {
			$this->error(self::INVALID);
			return false;
		}
		
		$this->setValue($value);
		
		if(!preg_match(self::PATTERN, $value)) {
			$this->error(self::NOT_MATCH);
		}
		
		$codes = array_unique(array_merge($this->currentCodes, $this->historicCodes));
		if(!in_array($value, $codes)) {
			$this->error(self::NOT_FOUND);
		}
		
		if(count($this->getMessages())) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * Return the array of valid currency codes
	 * @return array
	 */
	public function getValidCurrencyCodes() {
		return array_unique(array_merge($this->currentCodes, $this->historicCodes));
	}
	
}