<?php
/**
 * A Null Mail Transport. Send Mail messages absolutely nowhere!
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mail
 */

namespace Netglue\Mail\Transport;

use Zend\Mail\Transport\TransportInterface;
use Zend\Mail\Message;

/**
 * A Null Mail Transport. Send Mail messages absolutely nowhere!
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mail
 */
class Null implements TransportInterface {
	
	/**
	 * Send a Mail Message nowhere
	 * @param Message $message
	 * @return void
	 */
	public function send(Message $message) {
		return;
	}
	
}