<?php
/**
 * Mail - Invalid Argument Exception
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mail
 * @subpackage Exception
 */

namespace Netglue\Mail\Service\Exception;

/**
 * Mail - Invalid Argument Exception
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mail
 * @subpackage Exception
 */
class InvalidArgumentException extends \InvalidArgumentException {

}
