<?php
/**
 * Factory Service for bringing up Mail Transports from config
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mail
 */

namespace Netglue\Mail\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Config;
use Zend\Mail\Transport;

/**
 * Factory Service for bringing up Mail Transports from config
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012, Net Glue Ltd
 * @license http://opensource.org/licenses/mit-license.php MIT License
 * @category Zend
 * @package Netglue_Mail
 */
class TransportFactory implements FactoryInterface {
	
	/**
	 * The configuration key to look for in larger configuration contexts
	 */
	const CONFIG_KEY = 'mail_transport';
	
	/**
	 * Instantiated Transports, keyed by an md5 of the configuration
	 * @var array
	 */
	protected $_transports = array();
	
	/**
	 * Create the required Mail Transport from configuration
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return \Zend\Mail\Transport\TransportInterface
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config = $this->getTransportOptionsFromConfig($serviceLocator->get('Config'));
		$key = md5(serialize($config));
		if(isset($this->_transports[$key])) {
			return $this->_transports[$key];
		}
		return $this->createTransportFromConfig($config);
	}
	
	/**
	 * Create a mail transport from the provided configuration array
	 * @param array $config
	 * @return \Zend\Mail\Transport\TransportInterface
	 * @throws \Netglue\Mail\Service\Exception\InvalidArgumentException
	 */
	public function createTransportFromConfig(array $config) {
		if(!isset($config['class'])) {
			throw new Exception\InvalidArgumentException('No transport class has been provided in the mail_transport configuration');
		}
		$class = NULL;
		switch($config['class']) {
			
			case 'Zend\Mail\Transport\Sendmail':
			case 'Sendmail':
			case 'sendmail':
				$class = 'Zend\Mail\Transport\Sendmail';
				break;
			
			case 'Zend\Mail\Transport\Smtp':
			case 'Smtp':
			case 'smtp':
				$class = 'Zend\Mail\Transport\Smtp';
				break;
			
			case 'Zend\Mail\Transport\File':
			case 'File':
			case 'file':
				$class = 'Zend\Mail\Transport\File';
				break;
			
			case 'Netglue\Mail\Transport\Null':
			case 'Null':
			case 'null':
				$class = 'Netglue\Mail\Transport\Null';
				break;
			
			default:
				$class = $config['class'];
				break;
		}
		
		if(!class_exists($class)) {
			throw new Exception\InvalidArgumentException('Transport class provided '.$config['class'].' does not exist');
		}
		
		$options = isset($config['options']) ? $config['options'] : NULL;
		
		switch($class) {
			case 'Zend\Mail\Transport\Smtp':
				if(!is_null($options)) {
					$options = new Transport\SmtpOptions($options);
				}
				$transport = new Transport\Smtp($options);
				break;
			
			case 'Zend\Mail\Transport\Sendmail':
				$transport = new Transport\Sendmail($options);
				break;
			
			case 'Zend\Mail\Transport\File':
				if(!is_null($options)) {
					$options = new Transport\FileOptions($options);
				}
				$transport = new Transport\File($options);
				break;
			
			default:
				$transport = new $class;
				if(method_exists($transport, 'setOptions') && !is_null($options)) {
					$transport->setOptions($options);
				}
				break;
		}
		
		$key = md5(serialize($config));
		$this->_transports[$key] = $transport;
		return $transport;
	}
	
	/**
	 * Given some kind of configuration parameter, distill into an array and return just the options keyed with 'mail_transport'
	 * 
	 * @param string|\Zend\Config\Config|array $config
	 * @return array
	 * @throws \Netglue\Mail\Service\Exception\InvalidArgumentException
	 */
	public function getTransportOptionsFromConfig($config = null) {
		if(is_string($config)) {
			if(file_exists($config)) {
				/**
				 * @throws Zend\Config\Exception\InvalidArgumentException
				 * @return array
				 */
				$config = Config\Factory::fromFile($config);
			} else {
				throw new Exception\InvalidArgumentException(sprintf('A string was provided to mail transport configuration but does not seem to be a valid file on disk: %s', $config));
			}
		} elseif($config instanceof Config\Config) {
			$config = $config->toArray();
		} elseif(!is_array($config)) {
			throw new Exception\InvalidArgumentException('Invalid configuration input. Expected a file name, Zend\Config\Config object, or an array');
		}
		$key = self::CONFIG_KEY;
		if(!isset($config[$key]) || !is_array($config[$key])) {
			throw new Exception\InvalidArgumentException("Could not find the configuration key '{$key}'");
		}
		return $config[$key];
	}
	
	
}