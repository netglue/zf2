<?php

namespace NetglueTest\Filter;

use Netglue\Filter\HtmlToText;
use Netglue\Filter\Exception;

class HtmlToTextTest extends \PHPUnit_Framework_TestCase {
	
	/**
	 * @var \Netglue\Filter\HtmlToText
	 */
	protected $_filter;
	
	
	public function setUp() {
		$this->_filter = new HtmlToText;
	}
	
	/**
	 * Ensures that the validator follows expected behavior
	 *
	 * @return void
	 */
	public function testFragment() {
		$this->assertEquals('Some Text', trim($this->_filter->filter('<p>Some Text</p>')));
	}
	
	public function testNotString() {
		$this->setExpectedException('InvalidArgumentException');
		$this->_filter->filter(array());
	}
	
	public function testBasic() {
		$input = array(
			'<p>Some html</p>' => 'Some html',
			'<p>Some <br /><strong>More <em>Complex</em></strong> <acronym title="HTML">HTML</acronym></p>' => 'Some More Complex HTML',
			'Not HTML At All' => 'Not HTML At All',
			'<p>A unicode Euro €</p>' => 'A unicode Euro €',
		);
		foreach($input as $html => $expected) {
			$filtered = $this->_filter->filter($html);
			$this->assertEquals(true, is_string($filtered));
			$this->assertEquals($expected, trim($filtered));
		}
	}
	
	public function testEncoding() {
		$this->markTestSkipped('Need to figure out if the problem is mb_detect_encoding');
		return;
		$files = array(
			__DIR__.'/../../data/Filter/Latin1.html' => 'ISO-8859-1',
			__DIR__.'/../../data/Filter/UTF-8.html' => 'UTF-8',
		);
		mb_detect_order(implode(",", $files));
		foreach($files as $file => $encoding) {
			$data = file_get_contents($file);
			$this->assertEquals($encoding, mb_detect_encoding($data));
			$data = $this->_filter->filter($data);
			$this->assertEquals($encoding, mb_detect_encoding($data));
		}
	}
}
