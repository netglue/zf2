<?php

namespace NetglueTest\Filter;

use Netglue\Filter\StringPad;
use Netglue\Filter\Exception;

class StringPadTest extends \PHPUnit_Framework_TestCase {
	
	/**
	 * @var \Netglue\Filter\StringPad
	 */
	protected $filter;
	
	
	public function setUp() {
		$this->filter = new StringPad;
	}
	
	/**
	 * @covers \Netglue\Filter\StringPad::setPadString
	 * @covers \Netglue\Filter\StringPad::getPadString
	 */
	public function testSetPadString() {
		$return = $this->filter->setPadString('Foo');
		$this->assertSame($this->filter, $return);
		$this->assertSame('Foo', $this->filter->getPadString());
		
		$this->filter->setPadString(1);
		$this->assertSame('1', $this->filter->getPadString());
		
	}
	
	/**
	 * @covers \Netglue\Filter\StringPad::setPadLength
	 * @covers \Netglue\Filter\StringPad::getPadLength
	 */
	public function testSetPadLength() {
		$return = $this->filter->setPadLength(5);
		$this->assertSame($this->filter, $return);
		$this->assertSame(5, $this->filter->getPadLength());
		
		$values = array(
			'1' => 1,
			'10' => 10,
			10 => 10,
			1 => 1,
			'Foo' => 0,
		);
		
		foreach($values as $v => $expect) {
			$this->filter->setPadLength($v);
			$this->assertSame($expect, $this->filter->getPadLength());
		}
	}
	
	/**
	 * @covers \Netglue\Filter\StringPad::setPadType
	 * @covers \Netglue\Filter\StringPad::getPadType
	 */
	public function testSetPadType() {
		$return = $this->filter->setPadType(STR_PAD_LEFT);
		$this->assertSame($this->filter, $return);
		$this->assertSame(STR_PAD_LEFT, $this->filter->getPadType());
		
		$valid = array(
			'Left' => STR_PAD_LEFT,
			'left' => STR_PAD_LEFT,
			'LEFT' => STR_PAD_LEFT,
			'Right' => STR_PAD_RIGHT,
			'right' => STR_PAD_RIGHT,
			'RIGHT' => STR_PAD_RIGHT,
			'Both' => STR_PAD_BOTH,
			'both' => STR_PAD_BOTH,
			'BOTH' => STR_PAD_BOTH,
			STR_PAD_LEFT => STR_PAD_LEFT,
			STR_PAD_RIGHT => STR_PAD_RIGHT,
			STR_PAD_BOTH => STR_PAD_BOTH,
		);
		
		foreach($valid as $v => $expected) {
			$this->filter->setPadType($v);
			$this->assertSame($expected, $this->filter->getPadType());
		}
		
	}
	
	/**
	 * @expectedException Zend\Filter\Exception\InvalidArgumentException
	 */
	public function testSetInvalidOptions() {
		$opt = array(
			'foo' => 'bar',
		);
		$this->filter->setOptions($opt);
	}
	
	/**
	 * @depends testSetPadString
	 * @depends testSetPadLength
	 * @depends testSetPadType
	 * @covers \Netglue\Filter\StringPad::setOptions
	 * @covers \Netglue\Filter\StringPad::getOptions
	 */
	public function testSetValidOptions() {
		$options = array(
			'pad_string' => '1',
			'pad_length' => 10,
			'pad_type' => STR_PAD_LEFT,
		);
		$this->filter->setOptions($options);
		$out = $this->filter->getOptions();
		foreach($options as $key => $val) {
			$this->assertSame($val, $out[$key]);
		}
	}
	
	/**
	 * @covers \Netglue\Filter\StringPad::__construct
	 * @depends testSetValidOptions
	 */
	public function testConstructorOptions() {
		$options = array(
			'pad_string' => '1',
			'pad_length' => 10,
			'pad_type' => STR_PAD_LEFT,
		);
		$filter = new StringPad($options);
		$out = $filter->getOptions();
		foreach($options as $key => $val) {
			$this->assertSame($val, $out[$key]);
		}
	}
	
	/**
	 * @covers \Netglue\Filter\StringPad::filter
	 * @depends testConstructorOptions
	 */
	public function testFilter() {
		$options = array(
			'pad_string' => 'z',
			'pad_length' => 10,
			'pad_type' => STR_PAD_RIGHT,
		);
		$data = array(
			'A' => 'Azzzzzzzzz',
			'Foo' => 'Foozzzzzzz',
			100 => '100zzzzzzz',
			'1234567890' => '1234567890',
		);
		$filter = new StringPad($options);
		foreach($data as $value => $expected) {
			$this->assertSame($expected, $filter->filter($value));
		}
		// Empty String
		$this->assertSame('zzzzzzzzzz', $filter->filter(''));
		// NULL
		$this->assertSame('zzzzzzzzzz', $filter->filter(NULL));
		// false
		$this->assertSame('zzzzzzzzzz', $filter->filter(false));
		// true
		$this->assertSame('1zzzzzzzzz', $filter->filter(true));
		// array
		$this->assertSame('Arrayzzzzz', $filter->filter(array()));
		// float
		$this->assertSame('1.5zzzzzzz', $filter->filter(1.5));
		$this->assertSame('1.56789zzz', $filter->filter(1.56789));
	}
	
	
}
