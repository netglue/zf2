<?php

namespace NetglueTest\Validator;

use Netglue\Validator\UnCountryCode;

class UnCountryCodeTest extends \PHPUnit_Framework_TestCase {
	
	public function ccDataProvider() {
		return array(
			array('000', true),
			array('010', true),
			array('999', true),
		);
	}
	
	
	public function ccInvalidProvider() {
		return array(
			array('ABCDE', false),
			array('a', false),
			array('ZZZ', false),
			array(1, false),
			array(false, false),
			array(array(), false),
			array(0.5, false),
			array('0.5', false),
			array('€', false),
			array('01203', false),
		);
	}
	
	/**
	 * Ensures that the validator follows expected behavior
	 *
	 * @dataProvider ccDataProvider
	 * @return void
	 */
	public function testBasic($code, $expected) {
		$validator = new UnCountryCode();
		$this->assertEquals($expected, $validator->isValid($code), implode("\n", array_merge($validator->getMessages())));
	}
	
	
	/**
	 * Ensures that the validator follows expected behavior
	 *
	 * @dataProvider ccInvalidProvider
	 * @return void
	 */
	public function testInvalidCodes($code, $expected) {
		$validator = new UnCountryCode();
		$this->assertEquals($expected, $validator->isValid($code), implode("\n", array_merge($validator->getMessages())));
	}
	
}
