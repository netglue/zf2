<?php

namespace NetglueTest\Validator;

use Netglue\Validator\CurrencyCode;

class CurrencyCodeTest extends \PHPUnit_Framework_TestCase {
	
	public function ccDataProvider() {
		$v = new CurrencyCode;
		$codes = $v->getValidCurrencyCodes();
		$out = array();
		foreach($codes as $code) {
			$out[] = array($code, true);
		}
		return $out;
	}
	
	
	public function ccInvalidProvider() {
		return array(
			array('ABCDE', false),
			array('a', false),
			array('ZZZ', false),
			array(1, false),
			array(false, false),
			array(array(), false),
		);
	}
	
	/**
	 * Ensures that the validator follows expected behavior
	 *
	 * @dataProvider ccDataProvider
	 * @return void
	 */
	public function testBasic($code, $expected) {
		$validator = new CurrencyCode();
		$this->assertEquals($expected, $validator->isValid($code), implode("\n", array_merge($validator->getMessages())));
	}
	
	public function testEqualsMessageTemplates() {
		$validator = new CurrencyCode();
		$this->assertAttributeEquals(
			$validator->getOption('messageTemplates'),
			'messageTemplates',
			$validator
		);
	}
	
	/**
	 * Ensures that the validator follows expected behavior
	 *
	 * @dataProvider ccInvalidProvider
	 * @return void
	 */
	public function testInvalidCodes($code, $expected) {
		$validator = new CurrencyCode();
		$this->assertEquals($expected, $validator->isValid($code), implode("\n", array_merge($validator->getMessages())));
	}
	
}
